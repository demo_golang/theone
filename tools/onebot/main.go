package main

import (
	"fmt"
	"os"

	"github.com/slack-go/slack"
)

func main() {
	SLACK_TOKEN := os.Getenv("SLACK_TOKEN")
	api := slack.New(SLACK_TOKEN)
	attachment := slack.Attachment{
		Pretext: "some pretext",
		Text:    "some text 1",
		// Uncomment the following part to send a field too
		/*
			Fields: []slack.AttachmentField{
				slack.AttachmentField{
					Title: "a",
					Value: "no",
				},
			},
		*/
	}

	channelID, timestamp, err := api.PostMessage(
		"rentzy-group",
		slack.MsgOptionText("Some text", false),
		slack.MsgOptionAttachments(attachment),
		slack.MsgOptionAsUser(true), // Add this if you want that the bot would post message as a user, otherwise it will send response using the default slackbot
	)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	fmt.Printf("Message successfully sent to channel %s at %s", channelID, timestamp)
}
