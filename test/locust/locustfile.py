import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):

    @task
    def ping(self):
        self.client.get("/v1/ping")