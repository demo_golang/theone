# theone

This is a demo of monorepos for Golang training class

# Getting started

# Todo

- [x] Monorepo
- [x] GitOps
- [x] Integrate Bazel
- [x] Integrate Protobuf/GRPC Gateway V2
- [ ] Build Slack Bot
- [ ] API Testing using Python (Locust) base on Docker In docker / or container directly with Go using testcontainer
- [ ] Build Gitlab Bot for Owner/Approver/Merged
- [ ] Build Document: API, Rich text/MD doc, Database schema directly from migration files.
- [ ] Monitoring: Grafana/Prometheus/Loki
- [ ] APM: Jaeger
- [ ] Pub/Sub: Kafka/RabbitMQ
- [ ] Reactjs/Admin
- [ ] API Gateway Integration: Kong
- [ ] Nginx/HTTP Let's Encrypt
- [ ] AI/ML: Face Detection
- [ ] GRPC LB
- [ ] Code Quality: SonarQube, Golang CI
- [ ] Search Engine: ES Integration
- [ ] GraphDB: Neo4j Integration
- [ ] CDC: Debezium
- [ ] Python
- [ ] PHP
- [ ] Nodejs
- [ ] Java
- [ ] C#

# Demo

# HTTPs

https://github.com/nginx-proxy/acme-companion
https://github.com/nginx-proxy/nginx-proxy
https://cloud.google.com/community/tutorials/nginx-reverse-proxy-docker
