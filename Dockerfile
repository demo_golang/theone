FROM golang:1.16-alpine

# Export ENV
ENV GO111MODULE=on

# Install default common tool
RUN apk update  \
    && apk add curl nodejs npm git

# Install Nodejs Ecosystem
RUN npm install -g eslint yarn

# Install Golang Ecosystem
# googleapis/api-linter
RUN go get -u github.com/googleapis/api-linter/cmd/api-linter
# buf linter
RUN go get \
  github.com/bufbuild/buf/cmd/buf \
  github.com/bufbuild/buf/cmd/protoc-gen-buf-breaking \
  github.com/bufbuild/buf/cmd/protoc-gen-buf-lint

# Install soar
RUN apk add --update alpine-sdk
RUN  GO111MODULE=off go get -v github.com/XiaoMi/soar && cd ./src/github.com/XiaoMi/soar \
  && git checkout dev \
  && make install
