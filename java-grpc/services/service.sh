#!/bin/bash
# Start the first process
/app/grpc-gateway/target/ -D
status=$?
if [ $status -ne 0 ]; then
  java -jar grpc-gateway-1.0.jar
fi

# Start the second process
/app/grpc-service/target/ -D
status=$?
if [ $status -ne 0 ]; then
  java -jar grpc-service-1.0.jar
fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds

while sleep 60; do
  ps aux |grep grpc-gateway |grep -q -v grep
  PROCESS_1_STATUS=$?
  ps aux |grep grpc-service |grep -q -v grep
  PROCESS_2_STATUS=$?
  # If the greps above find anything, they exit with 0 status
  # If they are not both 0, then something is wrong
  if [ $PROCESS_1_STATUS -ne 0 -o $PROCESS_2_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit 1
  fi
done