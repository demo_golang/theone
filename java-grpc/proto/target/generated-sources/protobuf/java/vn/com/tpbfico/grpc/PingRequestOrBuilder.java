// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: api.proto

package vn.com.tpbfico.grpc;

public interface PingRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:PingRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>int64 timestamp = 1;</code>
   */
  long getTimestamp();
}
