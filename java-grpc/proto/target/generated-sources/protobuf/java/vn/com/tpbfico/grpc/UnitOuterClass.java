// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: unit.proto

package vn.com.tpbfico.grpc;

public final class UnitOuterClass {
  private UnitOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_CreateUnitRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_CreateUnitRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_UpdateUnitRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_UpdateUnitRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_GetUnitRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_GetUnitRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ListUnitRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ListUnitRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ListUnitResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ListUnitResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_Unit_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_Unit_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_UnitGroup_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_UnitGroup_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_CreateUnitGroupRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_CreateUnitGroupRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ListUnitGroupRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ListUnitGroupRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ListUnitGroupResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ListUnitGroupResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\nunit.proto\032\023common/common.proto\"\201\001\n\021Cr" +
      "eateUnitRequest\022\014\n\004code\030\001 \001(\t\022\014\n\004name\030\002 " +
      "\001(\t\022\023\n\013description\030\003 \001(\t\022\025\n\runit_group_i" +
      "d\030\004 \001(\r\022\021\n\tedited_by\030\005 \001(\t\022\021\n\tis_active\030" +
      "\006 \001(\010\"\215\001\n\021UpdateUnitRequest\022\n\n\002id\030\001 \001(\r\022" +
      "\014\n\004code\030\002 \001(\t\022\014\n\004name\030\003 \001(\t\022\023\n\013descripti" +
      "on\030\004 \001(\t\022\025\n\runit_group_id\030\005 \001(\r\022\021\n\tedite" +
      "d_by\030\006 \001(\t\022\021\n\tis_active\030\007 \001(\010\"\034\n\016GetUnit" +
      "Request\022\n\n\002id\030\001 \001(\r\"\201\001\n\017ListUnitRequest\022" +
      "\032\n\004sort\030\001 \001(\0132\014.common.Sort\022\037\n\007filters\030\002" +
      " \003(\0132\016.common.Filter\022\t\n\001q\030\003 \001(\t\022&\n\npagin" +
      "ation\030\004 \001(\0132\022.common.Pagination\"^\n\020ListU" +
      "nitResponse\022\r\n\005total\030\001 \001(\r\022&\n\npagination" +
      "\030\002 \001(\0132\022.common.Pagination\022\023\n\004data\030\003 \003(\013" +
      "2\005.Unit\"\240\001\n\004Unit\022\n\n\002id\030\001 \001(\r\022\014\n\004code\030\002 \001" +
      "(\t\022\014\n\004name\030\003 \001(\t\022\023\n\013description\030\004 \001(\t\022\025\n" +
      "\runit_group_id\030\005 \001(\r\022\021\n\tedited_by\030\006 \001(\t\022" +
      "\021\n\tis_active\030\007 \001(\010\022\036\n\nunit_group\030\n \001(\0132\n" +
      ".UnitGroup\"H\n\tUnitGroup\022\n\n\002id\030\001 \001(\r\022\014\n\004n" +
      "ame\030\002 \001(\t\022\023\n\013description\030\003 \001(\t\022\014\n\004code\030\006" +
      " \001(\t\";\n\026CreateUnitGroupRequest\022\014\n\004name\030\001" +
      " \001(\t\022\023\n\013description\030\002 \001(\t\"\206\001\n\024ListUnitGr" +
      "oupRequest\022\032\n\004sort\030\001 \001(\0132\014.common.Sort\022\037" +
      "\n\007filters\030\002 \003(\0132\016.common.Filter\022\t\n\001q\030\003 \001" +
      "(\t\022&\n\npagination\030\004 \001(\0132\022.common.Paginati" +
      "on\"h\n\025ListUnitGroupResponse\022\030\n\004data\030\001 \003(" +
      "\0132\n.UnitGroup\022\r\n\005total\030\002 \001(\r\022&\n\npaginati" +
      "on\030\003 \001(\0132\022.common.Pagination2\372\001\n\013UnitSer" +
      "vice\022!\n\007GetUnit\022\017.GetUnitRequest\032\005.Unit\022" +
      "\'\n\nUpdateUnit\022\022.UpdateUnitRequest\032\005.Unit" +
      "\022\'\n\nCreateUnit\022\022.CreateUnitRequest\032\005.Uni" +
      "t\0226\n\017CreateUnitGroup\022\027.CreateUnitGroupRe" +
      "quest\032\n.UnitGroup\022>\n\rListUnitGroup\022\025.Lis" +
      "tUnitGroupRequest\032\026.ListUnitGroupRespons" +
      "eB\027\n\023vn.com.tpbfico.grpcP\001b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          vn.com.tpbfico.grpc.common.Common.getDescriptor(),
        }, assigner);
    internal_static_CreateUnitRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_CreateUnitRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_CreateUnitRequest_descriptor,
        new java.lang.String[] { "Code", "Name", "Description", "UnitGroupId", "EditedBy", "IsActive", });
    internal_static_UpdateUnitRequest_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_UpdateUnitRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_UpdateUnitRequest_descriptor,
        new java.lang.String[] { "Id", "Code", "Name", "Description", "UnitGroupId", "EditedBy", "IsActive", });
    internal_static_GetUnitRequest_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_GetUnitRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_GetUnitRequest_descriptor,
        new java.lang.String[] { "Id", });
    internal_static_ListUnitRequest_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_ListUnitRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ListUnitRequest_descriptor,
        new java.lang.String[] { "Sort", "Filters", "Q", "Pagination", });
    internal_static_ListUnitResponse_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_ListUnitResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ListUnitResponse_descriptor,
        new java.lang.String[] { "Total", "Pagination", "Data", });
    internal_static_Unit_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_Unit_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_Unit_descriptor,
        new java.lang.String[] { "Id", "Code", "Name", "Description", "UnitGroupId", "EditedBy", "IsActive", "UnitGroup", });
    internal_static_UnitGroup_descriptor =
      getDescriptor().getMessageTypes().get(6);
    internal_static_UnitGroup_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_UnitGroup_descriptor,
        new java.lang.String[] { "Id", "Name", "Description", "Code", });
    internal_static_CreateUnitGroupRequest_descriptor =
      getDescriptor().getMessageTypes().get(7);
    internal_static_CreateUnitGroupRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_CreateUnitGroupRequest_descriptor,
        new java.lang.String[] { "Name", "Description", });
    internal_static_ListUnitGroupRequest_descriptor =
      getDescriptor().getMessageTypes().get(8);
    internal_static_ListUnitGroupRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ListUnitGroupRequest_descriptor,
        new java.lang.String[] { "Sort", "Filters", "Q", "Pagination", });
    internal_static_ListUnitGroupResponse_descriptor =
      getDescriptor().getMessageTypes().get(9);
    internal_static_ListUnitGroupResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ListUnitGroupResponse_descriptor,
        new java.lang.String[] { "Data", "Total", "Pagination", });
    vn.com.tpbfico.grpc.common.Common.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
