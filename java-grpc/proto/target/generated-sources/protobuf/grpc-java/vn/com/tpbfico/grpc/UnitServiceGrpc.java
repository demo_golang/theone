package vn.com.tpbfico.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.22.1)",
    comments = "Source: unit.proto")
public final class UnitServiceGrpc {

  private UnitServiceGrpc() {}

  public static final String SERVICE_NAME = "UnitService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.GetUnitRequest,
      vn.com.tpbfico.grpc.Unit> getGetUnitMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetUnit",
      requestType = vn.com.tpbfico.grpc.GetUnitRequest.class,
      responseType = vn.com.tpbfico.grpc.Unit.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.GetUnitRequest,
      vn.com.tpbfico.grpc.Unit> getGetUnitMethod() {
    io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.GetUnitRequest, vn.com.tpbfico.grpc.Unit> getGetUnitMethod;
    if ((getGetUnitMethod = UnitServiceGrpc.getGetUnitMethod) == null) {
      synchronized (UnitServiceGrpc.class) {
        if ((getGetUnitMethod = UnitServiceGrpc.getGetUnitMethod) == null) {
          UnitServiceGrpc.getGetUnitMethod = getGetUnitMethod = 
              io.grpc.MethodDescriptor.<vn.com.tpbfico.grpc.GetUnitRequest, vn.com.tpbfico.grpc.Unit>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "UnitService", "GetUnit"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.GetUnitRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.Unit.getDefaultInstance()))
                  .setSchemaDescriptor(new UnitServiceMethodDescriptorSupplier("GetUnit"))
                  .build();
          }
        }
     }
     return getGetUnitMethod;
  }

  private static volatile io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.UpdateUnitRequest,
      vn.com.tpbfico.grpc.Unit> getUpdateUnitMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "UpdateUnit",
      requestType = vn.com.tpbfico.grpc.UpdateUnitRequest.class,
      responseType = vn.com.tpbfico.grpc.Unit.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.UpdateUnitRequest,
      vn.com.tpbfico.grpc.Unit> getUpdateUnitMethod() {
    io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.UpdateUnitRequest, vn.com.tpbfico.grpc.Unit> getUpdateUnitMethod;
    if ((getUpdateUnitMethod = UnitServiceGrpc.getUpdateUnitMethod) == null) {
      synchronized (UnitServiceGrpc.class) {
        if ((getUpdateUnitMethod = UnitServiceGrpc.getUpdateUnitMethod) == null) {
          UnitServiceGrpc.getUpdateUnitMethod = getUpdateUnitMethod = 
              io.grpc.MethodDescriptor.<vn.com.tpbfico.grpc.UpdateUnitRequest, vn.com.tpbfico.grpc.Unit>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "UnitService", "UpdateUnit"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.UpdateUnitRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.Unit.getDefaultInstance()))
                  .setSchemaDescriptor(new UnitServiceMethodDescriptorSupplier("UpdateUnit"))
                  .build();
          }
        }
     }
     return getUpdateUnitMethod;
  }

  private static volatile io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.CreateUnitRequest,
      vn.com.tpbfico.grpc.Unit> getCreateUnitMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "CreateUnit",
      requestType = vn.com.tpbfico.grpc.CreateUnitRequest.class,
      responseType = vn.com.tpbfico.grpc.Unit.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.CreateUnitRequest,
      vn.com.tpbfico.grpc.Unit> getCreateUnitMethod() {
    io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.CreateUnitRequest, vn.com.tpbfico.grpc.Unit> getCreateUnitMethod;
    if ((getCreateUnitMethod = UnitServiceGrpc.getCreateUnitMethod) == null) {
      synchronized (UnitServiceGrpc.class) {
        if ((getCreateUnitMethod = UnitServiceGrpc.getCreateUnitMethod) == null) {
          UnitServiceGrpc.getCreateUnitMethod = getCreateUnitMethod = 
              io.grpc.MethodDescriptor.<vn.com.tpbfico.grpc.CreateUnitRequest, vn.com.tpbfico.grpc.Unit>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "UnitService", "CreateUnit"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.CreateUnitRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.Unit.getDefaultInstance()))
                  .setSchemaDescriptor(new UnitServiceMethodDescriptorSupplier("CreateUnit"))
                  .build();
          }
        }
     }
     return getCreateUnitMethod;
  }

  private static volatile io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.CreateUnitGroupRequest,
      vn.com.tpbfico.grpc.UnitGroup> getCreateUnitGroupMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "CreateUnitGroup",
      requestType = vn.com.tpbfico.grpc.CreateUnitGroupRequest.class,
      responseType = vn.com.tpbfico.grpc.UnitGroup.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.CreateUnitGroupRequest,
      vn.com.tpbfico.grpc.UnitGroup> getCreateUnitGroupMethod() {
    io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.CreateUnitGroupRequest, vn.com.tpbfico.grpc.UnitGroup> getCreateUnitGroupMethod;
    if ((getCreateUnitGroupMethod = UnitServiceGrpc.getCreateUnitGroupMethod) == null) {
      synchronized (UnitServiceGrpc.class) {
        if ((getCreateUnitGroupMethod = UnitServiceGrpc.getCreateUnitGroupMethod) == null) {
          UnitServiceGrpc.getCreateUnitGroupMethod = getCreateUnitGroupMethod = 
              io.grpc.MethodDescriptor.<vn.com.tpbfico.grpc.CreateUnitGroupRequest, vn.com.tpbfico.grpc.UnitGroup>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "UnitService", "CreateUnitGroup"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.CreateUnitGroupRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.UnitGroup.getDefaultInstance()))
                  .setSchemaDescriptor(new UnitServiceMethodDescriptorSupplier("CreateUnitGroup"))
                  .build();
          }
        }
     }
     return getCreateUnitGroupMethod;
  }

  private static volatile io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.ListUnitGroupRequest,
      vn.com.tpbfico.grpc.ListUnitGroupResponse> getListUnitGroupMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListUnitGroup",
      requestType = vn.com.tpbfico.grpc.ListUnitGroupRequest.class,
      responseType = vn.com.tpbfico.grpc.ListUnitGroupResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.ListUnitGroupRequest,
      vn.com.tpbfico.grpc.ListUnitGroupResponse> getListUnitGroupMethod() {
    io.grpc.MethodDescriptor<vn.com.tpbfico.grpc.ListUnitGroupRequest, vn.com.tpbfico.grpc.ListUnitGroupResponse> getListUnitGroupMethod;
    if ((getListUnitGroupMethod = UnitServiceGrpc.getListUnitGroupMethod) == null) {
      synchronized (UnitServiceGrpc.class) {
        if ((getListUnitGroupMethod = UnitServiceGrpc.getListUnitGroupMethod) == null) {
          UnitServiceGrpc.getListUnitGroupMethod = getListUnitGroupMethod = 
              io.grpc.MethodDescriptor.<vn.com.tpbfico.grpc.ListUnitGroupRequest, vn.com.tpbfico.grpc.ListUnitGroupResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "UnitService", "ListUnitGroup"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.ListUnitGroupRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.tpbfico.grpc.ListUnitGroupResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new UnitServiceMethodDescriptorSupplier("ListUnitGroup"))
                  .build();
          }
        }
     }
     return getListUnitGroupMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static UnitServiceStub newStub(io.grpc.Channel channel) {
    return new UnitServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static UnitServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new UnitServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static UnitServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new UnitServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class UnitServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getUnit(vn.com.tpbfico.grpc.GetUnitRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit> responseObserver) {
      asyncUnimplementedUnaryCall(getGetUnitMethod(), responseObserver);
    }

    /**
     */
    public void updateUnit(vn.com.tpbfico.grpc.UpdateUnitRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateUnitMethod(), responseObserver);
    }

    /**
     */
    public void createUnit(vn.com.tpbfico.grpc.CreateUnitRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateUnitMethod(), responseObserver);
    }

    /**
     */
    public void createUnitGroup(vn.com.tpbfico.grpc.CreateUnitGroupRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.UnitGroup> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateUnitGroupMethod(), responseObserver);
    }

    /**
     */
    public void listUnitGroup(vn.com.tpbfico.grpc.ListUnitGroupRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.ListUnitGroupResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getListUnitGroupMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetUnitMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                vn.com.tpbfico.grpc.GetUnitRequest,
                vn.com.tpbfico.grpc.Unit>(
                  this, METHODID_GET_UNIT)))
          .addMethod(
            getUpdateUnitMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                vn.com.tpbfico.grpc.UpdateUnitRequest,
                vn.com.tpbfico.grpc.Unit>(
                  this, METHODID_UPDATE_UNIT)))
          .addMethod(
            getCreateUnitMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                vn.com.tpbfico.grpc.CreateUnitRequest,
                vn.com.tpbfico.grpc.Unit>(
                  this, METHODID_CREATE_UNIT)))
          .addMethod(
            getCreateUnitGroupMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                vn.com.tpbfico.grpc.CreateUnitGroupRequest,
                vn.com.tpbfico.grpc.UnitGroup>(
                  this, METHODID_CREATE_UNIT_GROUP)))
          .addMethod(
            getListUnitGroupMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                vn.com.tpbfico.grpc.ListUnitGroupRequest,
                vn.com.tpbfico.grpc.ListUnitGroupResponse>(
                  this, METHODID_LIST_UNIT_GROUP)))
          .build();
    }
  }

  /**
   */
  public static final class UnitServiceStub extends io.grpc.stub.AbstractStub<UnitServiceStub> {
    private UnitServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private UnitServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UnitServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new UnitServiceStub(channel, callOptions);
    }

    /**
     */
    public void getUnit(vn.com.tpbfico.grpc.GetUnitRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetUnitMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateUnit(vn.com.tpbfico.grpc.UpdateUnitRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateUnitMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void createUnit(vn.com.tpbfico.grpc.CreateUnitRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateUnitMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void createUnitGroup(vn.com.tpbfico.grpc.CreateUnitGroupRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.UnitGroup> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateUnitGroupMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listUnitGroup(vn.com.tpbfico.grpc.ListUnitGroupRequest request,
        io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.ListUnitGroupResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getListUnitGroupMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class UnitServiceBlockingStub extends io.grpc.stub.AbstractStub<UnitServiceBlockingStub> {
    private UnitServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private UnitServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UnitServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new UnitServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public vn.com.tpbfico.grpc.Unit getUnit(vn.com.tpbfico.grpc.GetUnitRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetUnitMethod(), getCallOptions(), request);
    }

    /**
     */
    public vn.com.tpbfico.grpc.Unit updateUnit(vn.com.tpbfico.grpc.UpdateUnitRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdateUnitMethod(), getCallOptions(), request);
    }

    /**
     */
    public vn.com.tpbfico.grpc.Unit createUnit(vn.com.tpbfico.grpc.CreateUnitRequest request) {
      return blockingUnaryCall(
          getChannel(), getCreateUnitMethod(), getCallOptions(), request);
    }

    /**
     */
    public vn.com.tpbfico.grpc.UnitGroup createUnitGroup(vn.com.tpbfico.grpc.CreateUnitGroupRequest request) {
      return blockingUnaryCall(
          getChannel(), getCreateUnitGroupMethod(), getCallOptions(), request);
    }

    /**
     */
    public vn.com.tpbfico.grpc.ListUnitGroupResponse listUnitGroup(vn.com.tpbfico.grpc.ListUnitGroupRequest request) {
      return blockingUnaryCall(
          getChannel(), getListUnitGroupMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class UnitServiceFutureStub extends io.grpc.stub.AbstractStub<UnitServiceFutureStub> {
    private UnitServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private UnitServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UnitServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new UnitServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<vn.com.tpbfico.grpc.Unit> getUnit(
        vn.com.tpbfico.grpc.GetUnitRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetUnitMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<vn.com.tpbfico.grpc.Unit> updateUnit(
        vn.com.tpbfico.grpc.UpdateUnitRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateUnitMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<vn.com.tpbfico.grpc.Unit> createUnit(
        vn.com.tpbfico.grpc.CreateUnitRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateUnitMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<vn.com.tpbfico.grpc.UnitGroup> createUnitGroup(
        vn.com.tpbfico.grpc.CreateUnitGroupRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateUnitGroupMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<vn.com.tpbfico.grpc.ListUnitGroupResponse> listUnitGroup(
        vn.com.tpbfico.grpc.ListUnitGroupRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getListUnitGroupMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_UNIT = 0;
  private static final int METHODID_UPDATE_UNIT = 1;
  private static final int METHODID_CREATE_UNIT = 2;
  private static final int METHODID_CREATE_UNIT_GROUP = 3;
  private static final int METHODID_LIST_UNIT_GROUP = 4;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final UnitServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(UnitServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_UNIT:
          serviceImpl.getUnit((vn.com.tpbfico.grpc.GetUnitRequest) request,
              (io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit>) responseObserver);
          break;
        case METHODID_UPDATE_UNIT:
          serviceImpl.updateUnit((vn.com.tpbfico.grpc.UpdateUnitRequest) request,
              (io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit>) responseObserver);
          break;
        case METHODID_CREATE_UNIT:
          serviceImpl.createUnit((vn.com.tpbfico.grpc.CreateUnitRequest) request,
              (io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.Unit>) responseObserver);
          break;
        case METHODID_CREATE_UNIT_GROUP:
          serviceImpl.createUnitGroup((vn.com.tpbfico.grpc.CreateUnitGroupRequest) request,
              (io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.UnitGroup>) responseObserver);
          break;
        case METHODID_LIST_UNIT_GROUP:
          serviceImpl.listUnitGroup((vn.com.tpbfico.grpc.ListUnitGroupRequest) request,
              (io.grpc.stub.StreamObserver<vn.com.tpbfico.grpc.ListUnitGroupResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class UnitServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    UnitServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return vn.com.tpbfico.grpc.UnitOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("UnitService");
    }
  }

  private static final class UnitServiceFileDescriptorSupplier
      extends UnitServiceBaseDescriptorSupplier {
    UnitServiceFileDescriptorSupplier() {}
  }

  private static final class UnitServiceMethodDescriptorSupplier
      extends UnitServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    UnitServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (UnitServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new UnitServiceFileDescriptorSupplier())
              .addMethod(getGetUnitMethod())
              .addMethod(getUpdateUnitMethod())
              .addMethod(getCreateUnitMethod())
              .addMethod(getCreateUnitGroupMethod())
              .addMethod(getListUnitGroupMethod())
              .build();
        }
      }
    }
    return result;
  }
}
