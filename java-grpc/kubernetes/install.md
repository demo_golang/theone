1. Install Webservice 

```k apply -f deployment.yaml```

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/recommended.yaml

k apply -f dashboard-adminuser.yaml

kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"

kubectl proxy

http://localhost:8080/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/persistentvolumeclaim?namespace=default curl -O https://raw.githubusercontent.com/elastic/Helm-charts/master/elasticsearch/examples/minikube/values.yaml

Helm repo add elastic https://Helm.elastic.co

Helm install elasticsearch elastic/elasticsearch -f ./values.yaml
kubectl get pods --namespace=default -l app=elasticsearch-master
kubectl get svc
kubectl port-forward svc/elasticsearch-master 9200
Helm install kibana elastic/kibana
kubectl port-forward deployment/kibana-kibana 5601
http://127.0.0.1:5601/app/home#/
helm repo add stable https://charts.helm.sh/stable
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx