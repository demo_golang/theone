package vn.com.tpbfico.internal.service;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import vn.com.tpbfico.grpc.ApiServiceGrpc;
import vn.com.tpbfico.grpc.PingRequest;
import vn.com.tpbfico.grpc.PingResponse;

@GrpcService
public class ApiService extends ApiServiceGrpc.ApiServiceImplBase {

    @Override
    public void getPing(PingRequest request, StreamObserver<PingResponse> responseObserver) {
        long time = System.currentTimeMillis();
        PingResponse response = PingResponse.newBuilder().setMessage("reply from :" + (time - request.getTimestamp())).setTimestamp(time).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
