package vn.com.tpbfico.internal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vn.com.tpbfico.internal.entity.Units;


public interface UnitsRepository extends JpaRepository<Units, Integer>, JpaSpecificationExecutor<Units> {

}
