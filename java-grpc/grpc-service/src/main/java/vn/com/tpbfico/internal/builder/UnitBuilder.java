package vn.com.tpbfico.internal.builder;

import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.tpbfico.internal.entity.UnitGroups;
import vn.com.tpbfico.grpc.UnitGroup;

@Data
@NoArgsConstructor(staticName = "getInstance")
public class UnitBuilder {

    public UnitGroup toUnitGroupProto(UnitGroups unitGroups) {
        return UnitGroup.newBuilder()
                .setId(Math.toIntExact(unitGroups.getId()))
                .setDescription(unitGroups.getDescription())
                .setName(unitGroups.getName())
                .build();
    }
}

