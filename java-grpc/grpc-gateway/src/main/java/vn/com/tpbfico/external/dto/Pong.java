package vn.com.tpbfico.external.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(staticName = "getInstance")
public class Pong {

    private long timestamp;

    private String message;
}
