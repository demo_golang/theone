package vn.com.tpbfico.external.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(staticName = "getInstance")
@Builder
public class Status {
    private int code;
    private String message;
    private String detail;
}
