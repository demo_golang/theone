package vn.com.tpbfico.external.controller;

import io.grpc.StatusRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import vn.com.tpbfico.external.dto.Unit;
import vn.com.tpbfico.external.exception.ProcessException;
import vn.com.tpbfico.external.service.UnitService;

@RestController
public class UnitController {

    @Autowired
    UnitService unitService;

    @GetMapping(path = "/api/v1/unit/{id}")
    public ResponseEntity<Unit> getUnit(@PathVariable int id) throws StatusRuntimeException, ProcessException {
        Unit u = unitService.getUnit(id);
        return new ResponseEntity<>(u, HttpStatus.OK);
    }
}
