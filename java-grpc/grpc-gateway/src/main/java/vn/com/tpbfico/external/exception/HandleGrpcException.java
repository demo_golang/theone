package vn.com.tpbfico.external.exception;

import io.grpc.StatusRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import vn.com.tpbfico.external.dto.Status;
import vn.com.tpbfico.external.utilities.StringUtil;

@ControllerAdvice
public class HandleGrpcException {

    @ExceptionHandler(StatusRuntimeException.class)
    protected ResponseEntity<Status> handleGrpcError(StatusRuntimeException ex, WebRequest request) {
        Status status = Status.getInstance();
        status.setCode(!StringUtil.isEmpty(ex.getStatus().getCode().value()) ? ex.getStatus().getCode().value() : 404);
        status.setDetail(request.getContextPath());
        status.setMessage(!StringUtil.isEmpty(ex.getMessage()) ? ex.getMessage() : "");
        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ProcessException.class)
    protected ResponseEntity<Status> handleProcessError(ProcessException ex, WebRequest request) {
        Status status = ex.getStatus();
        status.setDetail(request.getContextPath());
        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }
}
