STATICLDFLAGS := -ldflags '-s -extldflags "-static"'
PWD = $(shell pwd)

install:
	cd /tmp && go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@latest
	cd /tmp && go install github.com/envoyproxy/protoc-gen-validate@v0.4.1
	cd /tmp && go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@latest
	cd /tmp && go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.26.0
	cd /tmp && go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.1.0
.PHONY: install

build:
	go install ./tools/onekit
.PHONY: build

update:
	go run ./tools/onekit protoc
.PHONY: update

up:
	docker-compose --compatibility --profile=dev up -d
.PHONY: up

init-on-server:
	# Step 1 create network first
	docker network create --driver bridge reverse-proxy
	# Step 2 up the nginx-proxy instance
	docker-compose --compatibility --profile=dev up -d
	# step 3 detach nginx-proxy-acme
	docker run --detach \
		--name nginx-proxy-acme \
		--volumes-from nginx-proxy \
		--volume /var/run/docker.sock:/var/run/docker.sock:ro \
		--volume acme:/etc/acme.sh \
		--env "DEFAULT_EMAIL=vietean@gmail.com" \
		nginxproxy/acme-companion
	
.PHONY: init-on-server
