package main

import (
	"github.com/labstack/gommon/log"
	"github.com/spf13/cobra"
	"theone.io/backend/core/internal/api/grpc"
)

var (
	rootCmd = &cobra.Command{
		Use:     "application",
		Short:   "Category Service",
		Long:    `Category Service`,
		Version: "1.0.0",
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.AddCommand(grpc.ServiceCmd)
}

func initConfig() {

}

func main() {
	err := rootCmd.Execute()
	if err != nil {
		log.Errorf("error while execute: %s", err)
	}
}
