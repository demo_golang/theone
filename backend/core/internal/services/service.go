package services

import (
	"context"

	"theone.io/backend/core/api"
	"theone.io/backend/core/internal/iam"
)

type Service interface {
	api.ApiServicesServer
}

type service struct {
	api.UnimplementedApiServicesServer
}

func Init() Service {
	return &service{}
}

func (s *service) getCurrentUser(ctx context.Context) iam.IUser {
	identity := ctx.Value(iam.Identity)
	var user iam.IUser
	if identity != nil {
		user = identity.(iam.IUser)
	}
	return user
}

func (s *service) Ping(ctx context.Context, in *api.PingRequest) (*api.PingResponse, error) {
	user := s.getCurrentUser(ctx)
	return &api.PingResponse{
		Message: "Hello, Toan, Huong, Diep... [2]" + user.GetName(),
	}, nil
}

func (s *service) Live(ctx context.Context, in *api.LiveRequest) (*api.LiveResponse, error) {
	return &api.LiveResponse{
		Message: "Pong",
	}, nil
}

func (s *service) AskTuanAnything(ctx context.Context, in *api.AskTuanAnythingRequest) (*api.AskTuanAnythingResponse, error) {
	return &api.AskTuanAnythingResponse{
		Reply: "Hello Tai, Thong, Nhan, Quang, Chuan, " + in.Message,
	}, nil
}

func (s *service) PatientAdd(ctx context.Context, req *api.PatientAddRequest) (*api.PatientAddResponse, error) {
	return &api.PatientAddResponse{
		Name:  "Xin chao :" + req.Name,
		Phone: req.Phone,
	}, nil
}

func (s *service) Welcome(ctx context.Context, req *api.WelcomeRequest) (*api.WelcomeResponse, error) {
	return &api.WelcomeResponse{
		Message: "Welcome :" + req.Name,
	}, nil
}
