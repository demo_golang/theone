package grpc

import (
	"context"
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"theone.io/backend/core/api"
	gw "theone.io/backend/core/internal/api/http"
	"theone.io/backend/core/internal/api/middleware"
	"theone.io/backend/core/internal/iam"
	"theone.io/backend/core/internal/services"
)

type GrpcServer interface {
	Run(ctx context.Context) error
}

type grpcServer struct {
	ctx    context.Context
	server *grpc.Server
	svc    services.Service
}

func Init(service services.Service) GrpcServer {
	var g grpcServer
	g.svc = service
	return &g
}

func (g *grpcServer) Run(ctx context.Context) error {
	g.ctx = ctx
	errChan := make(chan error)
	port := fmt.Sprintf(":%d", 10000)
	grpcListener, err := net.Listen("tcp", port)
	if err != nil {
		fmt.Println(err)
		return err
	}

	interceptors := middleware.NewInterceptor()
	type Validator interface {
		Validate() error
	}
	handler := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		if in, ok := req.(Validator); ok {
			if err := in.Validate(); err != nil {
				return nil, err
			}
		}
		return handler(ctx, req)
	}
	auth := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		user := iam.User{
			Id:   1,
			Name: "tpphu",
		}
		newContext := context.WithValue(ctx, iam.Identity, &user)
		return handler(newContext, req)
	}
	baseServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		interceptors.WithTimeoutInterceptor(),
	), grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(auth, handler)))

	api.RegisterApiServicesServer(baseServer, g.svc)
	g.server = baseServer
	go func() {
		errChan <- baseServer.Serve(grpcListener)
	}()

	//Start Gateway
	go func() {
		errChan <- gw.Start()
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()

	err = <-errChan
	g.server.GracefulStop()

	return err
}
