package http

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	gw "theone.io/backend/core/api"
)

func run() error {
	grpcPort := fmt.Sprintf("%d", 10000)
	httpPort := "8000"
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	serverMuxOption := runtime.WithIncomingHeaderMatcher(func(key string) (string, bool) {
		return key, true
	})

	mux := http.NewServeMux()
	gwMux := runtime.NewServeMux(serverMuxOption)
	opts := []grpc.DialOption{grpc.WithInsecure()}

	middlewareCORS := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			corsAllowOrigin := "*"
			if origin := req.Header.Get("Origin"); origin != "" {
				corsAllowOrigin = origin
			}

			w.Header().Set("Access-Control-Allow-Origin", corsAllowOrigin)
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, POST, GET, DELETE, PUT")
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, X-Requested-With")
			if req.Method == "OPTIONS" && req.Header.Get("Access-Control-Request-Method") != "" {
				return
			}
			next.ServeHTTP(w, req)
		})
	}

	err := gw.RegisterApiServicesHandlerFromEndpoint(ctx, gwMux, fmt.Sprintf(":%s", grpcPort), opts)
	if err != nil {
		return err
	}

	mux.Handle("/", middlewareCORS(gwMux))

	httpServer := &http.Server{
		Addr:        fmt.Sprintf(":%s", httpPort),
		Handler:     mux,
		BaseContext: func(_ net.Listener) context.Context { return ctx },
	}
	// Run server
	go func() {
		if err := httpServer.ListenAndServe(); err != http.ErrServerClosed {
			fmt.Println(err)
		}
	}()
	signalChan := make(chan os.Signal, 1)
	signal.Notify(
		signalChan,
		syscall.SIGHUP,  // kill -SIGHUP XXXX
		syscall.SIGINT,  // kill -SIGINT XXXX or Ctrl+c
		syscall.SIGQUIT, // kill -SIGQUIT XXXX
	)

	<-signalChan

	go func() {
		<-signalChan
	}()

	graceFullCtx, cancelShutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelShutdown()

	if err := httpServer.Shutdown(graceFullCtx); err != nil {
		defer os.Exit(1)
		return err
	}

	// manually cancel context if not using httpServer.RegisterOnShutdown(cancel)
	cancel()

	defer os.Exit(0)
	return nil
}

func Start() error {
	if err := run(); err != nil {
		return err
	}
	return nil
}
